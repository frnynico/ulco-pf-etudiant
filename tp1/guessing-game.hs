import System.Random

main :: IO ()
main = do
    target <- randomRIO(1, 100)
    run target

run :: Int -> IO ()
run target = do
    putStrLn("Vous devez saisir un nombre : ")
    line <- getLine
    let number = read line
    if number == target
    then putStrLn "You win !"
    else do
        putStrLn (if number < target then "Too Small!" else "Too big!")
        run target